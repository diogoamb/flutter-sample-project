import 'package:tech/index.dart';
import 'package:tech/bootstrap.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await OnePackageInfo.init();
  await StorageUtil.init();
  await ConnectivityUtil.init();
  FlavorConfig(
    environment: FlavorEnvironment.STAGING,
    name: STAGING_LABEL_NAME,
    variables: stagingEnvironment,
    color: Color(0xFF397CE4),
  );
  await bootstrap(() => App());
}
