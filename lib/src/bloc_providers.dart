import 'package:tech/index.dart';

class BlocProviders {
  static List<BlocProvider> providers = [
    BlocProvider<CounterCubit>(
      create: (context) => CounterCubit(),
    ),
  ];
}
