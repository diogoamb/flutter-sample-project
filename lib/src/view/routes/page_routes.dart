import 'package:tech/index.dart';

List<GetPage> get getPages {
  /// Get.toNamed() | pushNamed;
  /// Get.offNamed() | pushReplacementNamed;
  /// Get.offAllNamed() | pushNamedAndRemoveUntil;
  /// Get.offNamedUntil('home', (route) => false);
  /// static const String routeName = '/main_page';
  return [
    GetPage(
      name: SplashScreenPage.routeName,
      page: () => SplashScreenPage(),
    ),
    GetPage(
      name: CounterPage.routeName,
      page: () => CounterPage(),
    ),
    GetPage(
      name: HomePage.routeName,
      page: () => HomePage(),
    ),
    GetPage(
      name: VideoPlayPage.routeName,
      page: () => VideoPlayPage(),
    ),
    GetPage(
      name: LanguagePage.routeName,
      page: () => LanguagePage(),
    ),
    GetPage(
      name: SettingPage.routeName,
      page: () => BlocProvider<SettingCubit>(
        create: (context) => SettingCubit(),
        child: SettingPage(),
      ),
    ),
    GetPage(
      name: LoginPage.routeName,
      page: () => BlocProvider<LoginCubit>(
        create: (context) => LoginCubit(),
        child: LoginPage(),
      ),
    ),
  ];
}
