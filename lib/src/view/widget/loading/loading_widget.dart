import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tech/index.dart';

class OneLoadingWidget extends StatelessWidget {
  final EdgeInsets? padding;
  final Color? backgroundColor;
  final bool isWithScaffold;
  final bool isShowLeading;
  final AppBar? appBar;
  final String? appBarTitle;

  const OneLoadingWidget({
    Key? key,
    this.padding,
    this.backgroundColor,
    this.isWithScaffold: false,
    this.appBar,
    this.appBarTitle,
    this.isShowLeading: true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!isWithScaffold) {
      return _buildWidget;
    } else {
      return Scaffold(
        appBar: appBar ??
            AppBar(
              title: appBarTitle == null ? null : Text(appBarTitle!),
              leading: isShowLeading ? OneLeadingAppbar() : null,
            ),
        body: _buildWidget,
      );
    }
  }

  Widget get _buildWidget => OrientationBuilder(
        builder: (context, orientation) {
          final width = MediaQuery.of(context).size.width;
          final height = MediaQuery.of(context).size.height;
          final isPortrait = orientation == Orientation.portrait;
          return Container(
            color: backgroundColor ?? oneBackgroundColor(context),
            height: height,
            padding: padding,
            child: Center(
              child: Container(
                padding: EdgeInsets.all(5),
                margin: EdgeInsets.only(),
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Container(
                      height: 80,
                      width: 80,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                        // value: 50,
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 20,
                      child: OneWidgetSvgImage(
                        width: 40,
                        fit: BoxFit.contain,
                        path: OneSvg.logoNoText,
                        color: CustomColor.primary,
                        // color: Get.isDarkMode ? CustomColor.yellow : CustomColor.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // child: Center(
            //   child: Container(
            //     height: 120,
            //     width: 120,
            //     padding: EdgeInsets.all(20),
            //     margin: EdgeInsets.only(),
            //     child: Stack(
            //       alignment: AlignmentDirectional.center,
            //       children: <Widget>[
            //         Positioned.fill(
            //           left: 6,
            //           top: 6,
            //           child: Container(
            //             padding: EdgeInsets.all(10),
            //             child: SvgPicture.asset(
            //               'assets/icons/OneSala.svg',
            //               color: CustomColor.green,
            //             ),
            //           ),
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
          );
        },
      );
}
