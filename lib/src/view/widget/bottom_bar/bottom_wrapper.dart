import 'package:tech/index.dart';

Widget OneBottomWrapper({
  required Widget child,
  bool isBoxShadow = true,
  bool collapsed = false,
}) {
  return Builder(builder: (context) {
    double keyBordBottom = MediaQuery.of(context).viewInsets.bottom;
    double paddingBottom = MediaQuery.of(context).padding.bottom;
    return Container(
      decoration: BoxDecoration(
        // border: Border(
        //   top: BorderSide(width: 0.5, color: CustomColor.gray.withOpacity(.4)),
        // ),
        color: oneBackgroundColor(context),
        boxShadow: !isBoxShadow
            ? null
            : [
                BoxShadow(
                  blurRadius: 3,
                  color: Get.isDarkMode ? Colors.white.withOpacity(0.02) : Colors.black.withOpacity(0.05),
                  offset: Offset(0, -5),
                  spreadRadius: 1,
                ),
              ],
      ),
      child: OneCollapsible(
        collapsed: collapsed,
        child: Container(
          margin: EdgeInsets.only(bottom: keyBordBottom + paddingBottom, left: 16, right: 16),
          child: child,
        ),
      ),
    );
  });
}
