import 'package:tech/index.dart';

class OneLeadingAppbar extends StatelessWidget {
  final VoidCallback? onPressed;
  final bool isShapeCircle;
  const OneLeadingAppbar({
    Key? key,
    this.onPressed,
    this.isShapeCircle = false,
    this.svgPath,
  }) : super(key: key);

  final String? svgPath;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        OneGestureDetector(
          onTap: () {
            if (onPressed == null) {
              Get.back();
            } else {
              onPressed!();
            }
          },
          child: Container(
            margin: EdgeInsets.only(
              left: 15,
            ),
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
              color: isShapeCircle ? CustomColor.black.withOpacity(.25) : null,
              shape: BoxShape.circle,
            ),
            child: OneWidgetSvgImage(
              width: 29,
              color: isShapeCircle ? Colors.white : oneIconColor(context),
              fit: BoxFit.contain,
              path: svgPath ?? OneSvg.back,
            ),
          ),
        ),
      ],
    );
  }
}
