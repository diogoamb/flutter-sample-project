import 'package:tech/index.dart';

class CounterPage extends StatelessWidget {
  static const String routeName = '/CounterPage';

  const CounterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return CounterView();
  }
}

class CounterView extends StatelessWidget {
  const CounterView({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Scaffold(
      appBar: AppBar(title: Text(l10n.save)),
      body: const Center(child: CounterText()),
      bottomNavigationBar: OneBottomWrapper(
        child: Row(
          children: [
            Expanded(
              child: OneFormButton(
                text: "+",
                color: Colors.blue,
                onPressed: () => context.read<CounterCubit>().increment(),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: OneFormButton(
                text: "-",
                color: Colors.red,
                onPressed: () => context.read<CounterCubit>().decrement(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CounterText extends StatelessWidget {
  const CounterText({super.key});

  @override
  Widget build(BuildContext context) {
    final count = context.select((CounterCubit cubit) => cubit.state);
    return Text('$count',
        style: oneTextTheme(context).displayLarge?.copyWith(
              fontSize: 40,
            ));
  }
}
