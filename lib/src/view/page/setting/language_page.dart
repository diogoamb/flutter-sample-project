import 'package:tech/index.dart';

class LanguagePage extends StatefulWidget {
  const LanguagePage({Key? key}) : super(key: key);

  static const String routeName = "/language_page";

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  bool get _isKm => Get.locale == null ? false : (Get.locale!.languageCode == "km");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: OneLeadingAppbar(),
        title: Text(context.l10n.language),
      ),
      body: Column(
        children: [
          GestureDetector(
            onTap: () {
              StorageUtil.setString(LANGUAGE_KEY, 'en');
              Get.updateLocale(const Locale('en', 'EN'));
              //oneEventBus.fire(OneEventBusLanguageChange());
              Get.back();
              // oneReloadData(context);
            },
            child: settingRow(
              context,
              assetImg: OneImage.langEn,
              text: "English",
              customeWidget: !_isKm
                  ? Container(
                      width: 25,
                      height: 25,
                      child: Icon(
                        Icons.check,
                        color: oneIconColor(context),
                      ),
                    )
                  : null,
            ),
          ),
          GestureDetector(
            onTap: () {
              StorageUtil.setString(LANGUAGE_KEY, 'km');
              Get.updateLocale(const Locale('km', 'KM'));
              //oneEventBus.fire(OneEventBusLanguageChange());
              Get.back();
              // oneReloadData(context);
            },
            child: settingRow(
              context,
              assetImg: OneImage.langKm,
              text: "Khmer",
              customeWidget: _isKm
                  ? Container(
                      width: 25,
                      height: 25,
                      child: Icon(
                        Icons.check,
                        color: oneIconColor(context),
                      ),
                    )
                  : null,
            ),
          ),
        ],
      ),
    );
  }
}

Widget settingRow(BuildContext context, {required String text, Widget? customeWidget, required String assetImg}) {
  return Container(
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(
          width: 1,
          color: OneHelperColor.greyDividerColor(context),
        ),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          width: 10,
        ),
        Image.asset(
          assetImg,
          fit: BoxFit.cover,
          height: 23,
          width: 38,
        ),
        Expanded(
          child: Container(
            height: 58,
            margin: const EdgeInsets.only(left: 15),
            padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Text(text, style: oneTextTheme(context).titleSmall),
                ),
                if (customeWidget != null) customeWidget
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
