/// setting_page
import 'package:tech/index.dart';

class SettingPage extends StatefulWidget {
  static const String routeName = '/setting_page';

  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

///extends State<SettingPage>
class _SettingPageState extends State<SettingPage> {
  final _svgSize = 22.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: OneLeadingAppbar(),
        title: Text(context.l10n.setting),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 15),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          SettingMenuItemWidget(
            onTap: () {
              Get.toNamed(LanguagePage.routeName);
            },
            leading: OneWidgetSvgImage(
              fit: BoxFit.contain,
              path: OneSvg.world,
              height: _svgSize,
            ),
            title: context.l10n.language,
          ),
          SettingMenuItemWidget(
            onTap: () {},
            leading: OneWidgetSvgImage(
              fit: BoxFit.contain,
              path: OneSvg.shieldTick,
              height: _svgSize,
            ),
            title: context.l10n.privacyAndTerms,
          ),
          BlocBuilder<SettingCubit, SettingState>(
            builder: (context, state) {
              return SettingMenuItemWidget(
                leading: OneWidgetSvgImage(
                  fit: BoxFit.contain,
                  path: OneSvg.bell,
                  height: _svgSize,
                ),
                title: context.l10n.notification,
                trailing: state.stateStatus == OneStateStatus.loading
                    ? OneLoadingContainer(
                        margin: EdgeInsets.zero,
                      )
                    : SizedBox(
                        height: 20,
                        child: Switch(
                          activeColor: CustomColor.yellow,
                          value: state.isNotificationOn,
                          onChanged: (value) => context.read<SettingCubit>().onNotificationChanged(value),
                        ),
                      ),
              );
            },
          ),
          SettingMenuItemWidget(
            onTap: () {},
            leading: OneWidgetSvgImage(
              fit: BoxFit.contain,
              path: OneSvg.exclamationRound,
              height: _svgSize,
            ),
            title: context.l10n.aboutApp,
            hasDivider: true,
          ),
          // if (oneCheckLogined)
          SettingMenuItemWidget(
            onTap: () {
              OneConfirm(
                new OneDiaLogWidgetPara(
                  title: context.l10n.logOut,
                  buttonRightColor: Colors.red,
                  buttonLeftColor: Colors.black,
                  buttonRightTextStyle: oneTextTheme(context).labelLarge?.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                  onTapLeft: () {
                    Get.back();
                  },
                  onTapRight: () {
                    Get.back();
                    oneEventBus.fire(OneForeLogout(true));
                  },
                ),
              ).show(context);
            },
            leading: OneWidgetSvgImage(
              fit: BoxFit.contain,
              path: OneSvg.logout,
              height: _svgSize,
              color: CustomColor.red,
            ),
            title: context.l10n.logOut,
            textColor: CustomColor.red,
          ),
          SettingMenuItemWidget(
            leading: OneWidgetSvgImage(
              fit: BoxFit.contain,
              path: OneSvg.version,
              height: _svgSize,
              color: OneColors.grayDark(context),
            ),
            title: context.l10n.version + ' ${OnePackageInfo.version}',
            textColor: OneColors.grayDark(context),
          ),
        ],
      ),
    );
  }
}
