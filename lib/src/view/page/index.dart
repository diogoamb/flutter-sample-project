export 'counter/counter_page.dart';
export 'splash_screen/splash_screen_page.dart';
export 'video_play/video_play.dart';
export 'home/home_page.dart';
export 'auth/login_page.dart';
export 'setting/language_page.dart';
export 'setting/setting_page.dart';
