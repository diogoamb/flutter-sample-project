/// home_page
import 'package:one_package/form/option/option.dart';
import 'package:tech/index.dart';

class HomePage extends StatefulWidget {
  static const String routeName = '/home_page';

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

///extends State<HomePage>
class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return OneGestureDetector(
      onTap: () {
        OneHelperWidget.focusNew(context);
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(context.l10n.home),
          actions: [
            OneGestureDetector(
              onTap: () {
                Get.toNamed(SettingPage.routeName);
              },
              child: Icon(
                Icons.settings,
                color: oneIconColor(context),
              ),
            ),
            SizedBox(width: 16),
          ],
        ),
        // body: OneLoadingWidget(),
        body: RefreshIndicator(
          onRefresh: () async {},
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 16,
            ),
            child: OneListViewBuilder(
              children: [
                OneFormButton(
                  text: context.l10n.login,
                  onPressed: () {
                    Get.toNamed(LoginPage.routeName);
                  },
                ),
                OneFormGallery(
                  allowUploadType: OneAllowChooseGalleryType.both,
                  customBuilder: (BuildContext context, List<AssetEntity>? val) {
                    return OneFormButton(
                      transparent: true,
                      text: context.l10n.chooseImage,
                      icon: Icon(Icons.photo_camera_back, color: CustomColor.primary),
                      textStyle: oneTextTheme(context).labelLarge?.copyWith(
                            color: CustomColor.primary,
                          ),
                    );
                  },
                ),
                OneFormTextField(
                  text: context.l10n.phone,
                ),
                OneFormDate(
                  text: context.l10n.date,
                ),
                OneFormTime(
                  text: context.l10n.time,
                ),
                OneFormTextField(
                  text: context.l10n.address,
                  textArea: true,
                  controller: TextEditingController(
                    text: "ផ្ទះលេខ 237A ផ្លូវ  76CC ភូមិត្រពាំងថ្លឹង3 សង្កាត់ចោមចៅ1 ខណ្ឌ ពោធិ៍ សែន ជ័យ រាជធានីភ្នំពេញ",
                  ),
                ),
                OneFormOption(
                  text: context.l10n.options.toString().toFirstUppercase(),
                  onTap: () {
                    List<BottomSheetActionModel> list = [
                      BottomSheetActionModel(
                        text: context.l10n.male,
                        icon: Icons.radio_button_on_outlined,
                        callbackAction: () {
                          Get.back();
                        },
                      ),
                      BottomSheetActionModel(
                        text: context.l10n.female,
                        icon: Icons.radio_button_off,
                        callbackAction: () {
                          Get.back();
                        },
                      ),
                      BottomSheetActionModel(
                        text: context.l10n.cancel.toFirstUppercase(),
                        icon: Icons.close,
                        callbackAction: () {
                          Get.back();
                        },
                      ),
                    ];
                    oneBottomSheetAction(context, list: list);
                  },
                ),
                OneLoadingContainer(),
                Container(
                  height: 200,
                  child: OneLoadingWidget(),
                ),
              ],
            ).separated((context, index) => SizedBox(height: 15)),
          ),
        ),
      ),
    );
  }
}

/*
              SizedBox(height: 15),
              OneFormButton(
                color: CustomColor.primary,
                text: 'Stream Video Play',
                textStyle: oneTextThemeMethod(context).button?.copyWith(
                      color: CustomColor.white,
                    ),
                onPressed: () {
                  Get.toNamed(VideoPlayPage.routeName);
                },
              ),*/
