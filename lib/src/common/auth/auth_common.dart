import 'package:tech/index.dart';

class OneAuthCommon {
  static bool checkAction(BuildContext context) {
    if (oneCheckLogined) {
      return true;
    } else {
      OneHelperDialog.showMessage(
        context,
        para: OneDiaLogWidgetPara(
          message: context.l10n.pleaseLogin,
          title: context.l10n.warningMsg,
          buttonLeftText: context.l10n.cancel,
          buttonRightText: context.l10n.login,
          buttonRightColor: CustomColor.primary,
          buttonLeftColor: Colors.black,
          buttonRightTextStyle: oneTextTheme(context).labelLarge?.copyWith(
                fontWeight: FontWeight.w600,
              ),
          onTapRight: () {
            Get.back();
            Get.toNamed(LoginPage.routeName);
          },
          onTapLeft: () {
            Get.back();
          },
        ),
      );
      return false;
    }
  }
}
