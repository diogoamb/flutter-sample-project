import 'package:tech/index.dart';

class OneConfirm {
  final OneDiaLogWidgetPara para;
  const OneConfirm(this.para);

  void show(BuildContext context) {
    oneModalBottomSheet(
      context,
      autoHeight: true,
      scrollAble: false,
      close: false,
      build: Builder(builder: (context) {
        return Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    if (para.title != null) ...[
                      Text(
                        para.title!,
                        style: oneTextTheme(context).displayLarge,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                    Text(
                      para.message ?? context.l10n.areYouSure,
                      style: oneTextTheme(context).titleSmall,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
              OneBottomWrapper(
                isBoxShadow: false,
                child: Row(
                  children: [
                    Expanded(
                      child: OneFormButton(
                        color: para.buttonLeftColor,
                        text: para.buttonLeftText ?? context.l10n.no,
                        textStyle: para.buttonLeftTextStyle ?? oneTextTheme(context).labelLarge,
                        onPressed: para.onTapLeft,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: OneFormButton(
                        color: para.buttonRightColor,
                        text: para.buttonRightText ?? context.l10n.yes,
                        textStyle: para.buttonRightTextStyle ?? oneTextTheme(context).labelLarge,
                        onPressed: para.onTapRight,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
