class OneSvg {
  static String logo = 'assets/svg/logo.svg';
  static String logoNoText = 'assets/svg/logo_no_text.svg';
  static String world = 'assets/svg/world.svg';
  static String shieldTick = 'assets/svg/shield_tick.svg';
  static String bell = 'assets/svg/bell.svg';
  static String exclamationRound = 'assets/svg/exclamation_round.svg';
  static String logout = 'assets/svg/logout.svg';

  static String back = 'assets/svg/back.svg';
  static String version = 'assets/svg/version.svg';
}

class OneSvgHeader {
  static String mainLogo = 'assets/_svg/logo_main.svg';
}

class OneSvgNotification {
  static const String eye = 'assets/_svg/notification/eye.svg';
}

class OneSvgNavBottom {
  static String qrCode = 'assets/_svg/nav_bottom/qr_code.svg';
  static String home = 'assets/_svg/nav_bottom/home.svg';
}

class OneSvgMedia {}

class OneImage {
  static String defaultProfile = 'assets/_images/default_profile.png';
  static String missingPicture = 'assets/_images/missing-picture.png';
  static String langEn = 'assets/images/lang/english.png';
  static String langKm = 'assets/images/lang/khmer.png';
  static String noInternetConnection = 'assets/_images/no_internet_connection.png';
  static String searchEmpty = 'assets/_images/search_empty.png';
  static String searchEngine = 'assets/_images/search_engine.png';
}
