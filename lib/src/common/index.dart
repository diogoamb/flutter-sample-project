import 'package:tech/index.dart';

export 'constants/environment.dart';
export 'constants/one_helper.dart';
export 'constants/path.dart';
export 'constants/string.dart';
export 'constants/one_icons_image.dart';
export 'themes/theme_method.dart';
export 'themes/dark_theme_data.dart';
export 'themes/light_theme_data.dart';
export 'one_package/one_package.dart';
export 'package_info/one_package_info.dart';
export 'flavors/flavor_config.dart';
export 'Utils/storage_util.dart';
export 'types/one_type.dart';
export 'http/http.dart';
export 'auth/auth_common.dart';
export 'confirm/comfirm.dart';
export 'notification/notification_common.dart';
export 'evenBus/event_bus_model.dart';
export 'Utils/connectivity.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;
import 'package:logger/logger.dart';

enum OneStateStatus { none, initial, loading, failure, empty, success, noInternet }

EdgeInsets oneGetAppBarPadding(BuildContext context) {
  return MediaQuery.of(context).padding;
}

var logger = Logger(
  printer: PrettyPrinter(
      methodCount: 1,
      // number of method calls to be displayed
      errorMethodCount: 0,
      // number of method calls if stacktrace is provided
      lineLength: 120,
      // width of the output
      colors: true,
      // Colorful log messages
      printEmojis: true,
      // Print an emoji for each log message
      printTime: false // Should each log print contain a timestamp
      ),
);

void log(message) {
  if (kDebugMode) logger.w(message);
}

bool get oneCheckLogined {
  String token = StorageUtil.getString(ACCESS_TOKEN);
  if (token == '') {
    return false;
  }
  return true;
}
