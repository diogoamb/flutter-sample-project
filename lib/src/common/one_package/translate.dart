import 'package:tech/index.dart';

/// Khmer Messages
class OneTranslate implements LookupTranslate {
  @override
  String wordSeparator() => ' ';

  @override
  String get date => oneG.context.l10n.date;

  @override
  String get gallery => oneG.context.l10n.gallery;

  @override
  String get camera => oneG.context.l10n.camera;

  @override
  String get cancel => oneG.context.l10n.cancel;

  @override
  String get addImage => oneG.context.l10n.addImage;

  @override
  String get edit => oneG.context.l10n.edit;

  @override
  String get time => oneG.context.l10n.time;

  @override
  String get close => oneG.context.l10n.close;

  @override
  String get permissionDenied => oneG.context.l10n.permissionDenied;

  @override
  String get permission => oneG.context.l10n.permission;

  @override
  String get pleaseAllowPermissionToAccess => oneG.context.l10n.pleaseAllowPermissionToAccess;

  @override
  String get yes => oneG.context.l10n.yes;

  @override
  String get no => oneG.context.l10n.no;

  @override
  String get ok => oneG.context.l10n.ok;

  @override
  String get off => oneG.context.l10n.off;

  @override
  String get action => oneG.context.l10n.action;

  @override
  String get warningMsg => oneG.context.l10n.warningMsg;

  @override
  String get sthUn => oneG.context.l10n.somethingUnexpectedWentWrong;

  @override
  String get options => oneG.context.l10n.options;

  @override
  String get normal => oneG.context.l10n.normal;

  @override
  String get subtitles => oneG.context.l10n.subtitles;

  @override
  String get disconnect => oneG.context.l10n.disconnect;

  @override
  String get connectToADevice => oneG.context.l10n.connectToADevice;

  @override
  String get searchingForDevices => oneG.context.l10n.searchingForDevices;

  @override
  String get noDeviceFound => oneG.context.l10n.noDeviceFound;

  @override
  String get noInternet => oneG.context.l10n.noInternetConnection;

  @override
  String get timeout => oneG.context.l10n.timeout;
}
