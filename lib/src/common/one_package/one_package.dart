// import 'package:superlive_player/superlive_player.dart';
import 'package:tech/index.dart';
import 'package:tech/src/common/one_package/translate.dart';

class RegisterOnePackage {
  static final RegisterOnePackage _instance = RegisterOnePackage._internal();
  factory RegisterOnePackage() => _instance;
  RegisterOnePackage._internal();

  Future<RegisterOnePackage> init() async {
    /* superlivePlayerInitialize(
      primaryColor: Color(0xFFe38246),
      logoAssetPath: 'assets/images/FinalLogo01.png',
      logoWidth: 40.0,
    );*/

    /*drPlayerInitialize(
      primaryColor: Color(0xFFe38246),
      logoAssetPath: 'assets/images/FinalLogo01.png',
      logoWidth: 40.0,
      */ /*configVideoModels : ConfigVideoModel().copyWith(
        videoLogo: '',
        arrowDownAssetImage: 'assets/icons/player/arrow-down.png',
        arrowLeftAssetImage: 'assets/icons/player/arrow-left-light.png',
        exitFullscreenAssetImage: 'assets/icons/player/exit-fullscreen.png',
        forwardAssetImage: 'assets/icons/player/fast-forward.png',
        fullscreenAssetImage: 'assets/icons/player/fullscreen.png',
        pauseAssetImage: 'assets/icons/player/pause.png',
        playAssetImage: 'assets/icons/player/play-black-512px.png',
        backWardAssetImage: 'assets/icons/player/rewind.png',
        replayAssetImage: 'assets/icons/player/replay.png',
        quality: 'assets/images/video_player_icons/1_quality.svg',
        audio: 'assets/images/video_player_icons/2_audio.svg',
        subtitles: 'assets/images/video_player_icons/3_subtitles.svg',
        speed: 'assets/images/video_player_icons/4_speed.svg',
        fillScreenDefault: 'assets/images/video_player_icons/8.1_fillscreen_default.svg',
        fillScreenActive: 'assets/images/video_player_icons/8.2_fillscreen_active.svg',
      ),
       */ /*
    );*/
    oneInitialize(
      defaultImageAsset: "",
      formatDate: "d/M/y",
      isDebug: OneVariable.isDebug,
      //isImageDebug: true,
      oneFilePath: OneFilePath().copyWith(
        errorAnimate: "assets/lotties/one_error_animate.json",
        warningAnimate: "assets/lotties/one_warning_animate.json",
        successAnimate: "assets/lotties/one_success_animate.json",
        success2Animate: "assets/lotties/upgrade_account_success.json",
      ),
    );
    setLocaleMessages(OneTranslate());
    return this;
  }
}
