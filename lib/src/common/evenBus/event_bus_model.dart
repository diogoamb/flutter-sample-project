import 'package:flutter/material.dart';

class OneEventBusUserChange {
  const OneEventBusUserChange();
}

class OneEventBusUserChangePictureProfile {
  const OneEventBusUserChangePictureProfile();
}

class OneEventBusLanguageChange {
  const OneEventBusLanguageChange();
}

class OneEventMainPageMoveToTop {
  final int index;
  const OneEventMainPageMoveToTop(this.index);
}

class OneEventBusScrollCollapsible {
  final bool collapsed;
  const OneEventBusScrollCollapsible({this.collapsed: false});
}

class OneEventBusConnectivity {
  final bool isReconnected;
  const OneEventBusConnectivity({this.isReconnected: true});
}
