import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:tech/index.dart';

class LocalNotification {
  LocalNotification._internal();
  static final _instance = LocalNotification._internal();
  factory LocalNotification() => _instance;

  Future<void> initial({
    required SelectNotificationCallback onSelectNotification,
    required DidReceiveLocalNotificationCallback onDidReceiveLocalNotification,
  }) async {
    const android = AndroidInitializationSettings('@mipmap/ic_launcher');
    final ios = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    final settings = InitializationSettings(android: android, iOS: ios);
    await FlutterLocalNotificationsPlugin().initialize(
      settings,
      onSelectNotification: onSelectNotification,
    );
    await _requestLocalNotificationPermissions();
  }

  Future<void> _requestLocalNotificationPermissions() async {
    /// For IOS
    await FlutterLocalNotificationsPlugin().resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()?.requestPermissions(alert: true, badge: true, sound: true);

    /// For Android
    await FlutterLocalNotificationsPlugin().resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(
          AndroidNotificationChannel(
            'default_notification_channel_id', // id
            'High Importance Notifications', // title // description
            importance: Importance.max,
            playSound: true,
          ),
        );
  }

  Future<void> showNotification(ShowNotificationModel notificationModel) async {
    const platformSpecifics = NotificationDetails(
      android: AndroidNotificationDetails(
        'default_notification_channel_id',
        'your channel name',
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker',
      ),
    );
    await FlutterLocalNotificationsPlugin().show(
      notificationModel.id,
      notificationModel.title,
      notificationModel.body,
      platformSpecifics,
      payload: notificationModel.payload,
    );
  }
}
