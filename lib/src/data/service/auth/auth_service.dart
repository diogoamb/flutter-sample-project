import 'package:dartz/dartz.dart';
import 'package:tech/index.dart';

class AuthService {
  OneResponseEither<LoginResModel> login(LoginReqModel req) async {
    try {
      final value = await OneHttp(
        url: OnePath.LOGIN,
        addToken: false,
        fields: req.toJson(),
      ).post();
      var response = OneResponse<LoginResModel>.fromJson(value, (data) => LoginResModel.fromJson(data));
      return right(response.data!);
    } catch (e) {
      print(e);
      return left(e.toString());
    }
  }
}
