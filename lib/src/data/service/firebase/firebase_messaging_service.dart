import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:tech/index.dart';

class FirebaseMessagingService {
  FirebaseMessagingService._internal();

  static final _instance = FirebaseMessagingService._internal();

  factory FirebaseMessagingService() => _instance;

  String? token;

  Future<void> setupNotification(BuildContext context) async {
    await _openingApp(context);
    await _openFromTerminatedApp();
    await _openFromBackgroundApp();
  }

  final _messageTopic = <String>[
    "all",
  ];

  Future<void> subscribeNotification(bool statusChanged) async {
    if (statusChanged) {
      await this.getDeviceToken();
      print("FIREBASE TOKEN:::::::: $token");
      // loop list of _messageTopic
      _messageTopic.forEach((element) async {
        print("FIREBASE SUBSCRIBE::::: $element");
        await FirebaseMessaging.instance.subscribeToTopic(element);
      });
    } else {
      await this.turnOff();
      _messageTopic.forEach((element) async {
        print("FIREBASE UNSUBSCRIBE::::: $element");
        await FirebaseMessaging.instance.unsubscribeFromTopic(element);
      });
    }
  }

  Future turnOff() async {
    token = null;
    //await FirebaseMessaging.instance.setAutoInitEnabled(false);
    await FirebaseMessaging.instance.deleteToken();
  }

  Future<String> getDeviceToken() async {
    token = await FirebaseMessaging.instance.getToken();
    if (token != null) {
      return token!;
    } else {
      throw oneG.context.l10n.somethingUnexpectedWentWrong;
    }
  }

  Stream<String> get deviceTokenRefresh async* {
    String _token = '';
    FirebaseMessaging.instance.onTokenRefresh.listen((event) {
      _token = event;
    });
    yield _token;
  }

  Future<void> _openingApp(BuildContext context) async {
    await FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log('Notification via foreground mode ============================>');
      log('remote message::: ${message.data}');
      _localNotification(
        context,
        id: 99,
        data: message.data,
        title: '${message.notification?.title}',
        body: "${message.notification?.body}",
      );
      /*oneEventBus.fire(EvenBusNotificationEvent(
        title: message.notification?.title,
        body: message.notification?.body,
        data: message.data,
        type: EnumNotificationActionType.onOpenedAppPushed,
      ));*/
    });
  }

  Future<void> _openFromTerminatedApp() async {
    final initialMessage = await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      log('Notification via Terminated mode ============================>');
      log('remote message::: ${initialMessage.data}');
      oneEventBus.fire(EvenBusNotificationEvent(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        data: initialMessage.data,
        type: EnumNotificationActionType.onBannerStatusBarPressed,
      ));
    }
  }

  Future<void> _openFromBackgroundApp() async {
    await FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('Notification via Background mode ============================>');
      log('remote message::: ${message.data}');
      oneEventBus.fire(EvenBusNotificationEvent(
        title: message.notification?.title,
        body: message.notification?.body,
        data: message.data,
        type: EnumNotificationActionType.onBannerStatusBarPressed,
      ));
    });
  }

  Future<NotificationSettings> requestIOSPermissions() async {
    NotificationSettings settings = await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: true,
      sound: true,
    );
    return settings;
  }

  Future<AuthorizationStatus> getNotificationAuthZStatus(NotificationSettings settings) async {
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
      return AuthorizationStatus.authorized;
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      print('User granted provisional permission');
      return AuthorizationStatus.provisional;
    } else {
      print('User declined or has not accepted permission');
      return AuthorizationStatus.denied;
    }
  }

  static void _localNotification(
    context, {
    required Map<String, dynamic> data,
    required String title,
    required String body,
    required int id,
  }) {
    var localNotification = AppService.instance.get<LocalNotification>();
    final notificationModel = ShowNotificationModel(
      id: id,
      title: '${title}',
      body: '${body}',
      payload: json.encode(data),
    );
    localNotification.initial(onSelectNotification: (String? payload) {
      log('Notification via localNotification mode :::::::::::::::::::::::::::');
      oneEventBus.fire(EvenBusNotificationEvent(
        title: title,
        body: body,
        data: data,
        type: EnumNotificationActionType.onLocalBannerStatusBarPressed,
      ));
    }, onDidReceiveLocalNotification: (int id, String? _title, String? _body, String? payload) {
      log('Notification via onDidReceiveLocalNotification mode :::::::::::::::::::::::::::');
      oneEventBus.fire(EvenBusNotificationEvent(
        title: title,
        body: body,
        data: data,
        type: EnumNotificationActionType.onLocalBannerStatusBarPressed,
      ));
    });
    localNotification.showNotification(notificationModel);
  }
}

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}
