import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:tech/index.dart';

class ShowNotificationModel {
  final int id;
  final String title;
  final String body;
  final String payload;

  ShowNotificationModel({
    required this.id,
    required this.title,
    required this.body,
    required this.payload,
  });
}

class EvenBusNotificationEvent {
  final String? title;
  final String? body;
  final Map<String, dynamic>? data;
  final EnumNotificationActionType type;
  const EvenBusNotificationEvent({
    required this.title,
    required this.body,
    required this.data,
    this.type: EnumNotificationActionType.onOpenedAppPushed,
  });
}

class FirebaseMessagingEvent {
  final String type;

  FirebaseMessagingEvent({required this.type});
}
