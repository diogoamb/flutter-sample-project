import 'package:tech/index.dart';

class LoginReqModel {
  final String username;
  final String password;
  final String? countryCode;

  LoginReqModel({
    required this.username,
    required this.password,
    this.countryCode,
  });

  Map<String, dynamic> toJson() {
    final _json = {
      "username": (countryCode != null ? "+" + countryCode! : "") + username.toString().toOneRemoveFirstZero(),
      "password": password,
    };
    print(_json);
    return _json;
  }
}
