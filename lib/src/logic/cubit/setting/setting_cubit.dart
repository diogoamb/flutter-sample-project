import 'dart:io';
import 'package:notification_permissions/notification_permissions.dart';
import 'package:tech/index.dart';
part 'setting_state.dart';

class SettingCubit extends Cubit<SettingState> {
  SettingCubit() : super(SettingState());

  final DeviceTokenService _service = AppService.instance.get<DeviceTokenService>();
  final FirebaseMessagingService _serviceFirebaseMessaging = AppService.instance.get<FirebaseMessagingService>();

  Future<void> initialNotificationPermission() async {
    emit(state.copyWith(
      stateStatus: OneStateStatus.loading,
    ));
    try {
      final isOn = StorageUtil.getBool(IS_NOTIFICATION_ON, true);
      final permissionStatus = await _getCurrentNotificationStatus();
      if (isOn) {
        _setNotification(true);
      }
      print("Notification ::::::: isOn: $isOn");
      if (permissionStatus == PermissionStatus.granted) {
        print("Notification ::::::: isGranted: true");
        emit(state.copyWith(
          isNotificationOn: isOn,
          isNotificationGranted: true,
        ));
      } else {
        print("Notification ::::::: isGranted: off");
        emit(state.copyWith(
          isNotificationOn: false,
          isNotificationGranted: false,
        ));
      }
      emit(state.copyWith(
        stateStatus: OneStateStatus.success,
      ));
    } catch (e) {
      emit(state.copyWith(
        stateStatus: OneStateStatus.failure,
      ));
    }
  }

  Future _setNotification(bool value) async {
    await _serviceFirebaseMessaging.subscribeNotification(value);
    StorageUtil.setBool(IS_NOTIFICATION_ON, value);
    emit(state.copyWith(
      isNotificationOn: value,
      isNotificationGranted: value,
    ));
  }

  void onNotificationChanged(value) async {
    emit(state.copyWith(
      stateStatus: OneStateStatus.loading,
    ));
    final notification = await _getCurrentNotificationStatus();
    bool result = value;
    if (value) {
      if (notification == PermissionStatus.granted) {
        result = true;
      } else {
        final permissionStatus = _requestNotificationPermission();
        if (permissionStatus == PermissionStatus.granted) {
          result = true;
        } else {
          result = false;
        }
      }
    } else {
      result = false;
    }
    String? deviceToken = _serviceFirebaseMessaging.token;
    await _setNotification(result);
    if (oneCheckLogined) {
      try {
        if (result) {
          String _deviceToken = _serviceFirebaseMessaging.token!;
          print(_deviceToken);
          await _service.saveDeviceToken(_deviceToken);
        } else {
          print(deviceToken);
          await _service.deleteDeviceToken(deviceToken);
        }
      } catch (e) {
        print(e);
      }
    }
    emit(state.copyWith(
      stateStatus: OneStateStatus.success,
    ));
  }

  Future<PermissionStatus> _getCurrentNotificationStatus() async {
    return await NotificationPermissions.getNotificationPermissionStatus();
  }

  Future<PermissionStatus> _requestNotificationPermission() async {
    if (Platform.isAndroid) {
      final result = await NotificationPermissions.requestNotificationPermissions(
        openSettings: true,
      );
      return result;
    } else {
      final result = await NotificationPermissions.requestNotificationPermissions(
        iosSettings: const NotificationSettingsIos(
          sound: true,
          badge: true,
          alert: true,
        ),
      );
      return result;
    }
  }
}
