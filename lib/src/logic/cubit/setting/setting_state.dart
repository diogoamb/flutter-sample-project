part of 'setting_cubit.dart';

class SettingState {
  final OneStateStatus? stateStatus;
  final bool isNotificationGranted;
  final bool isNotificationOn;
  SettingState({
    this.stateStatus,
    this.isNotificationGranted: false,
    this.isNotificationOn: false,
  });

  SettingState copyWith({
    OneStateStatus? stateStatus,
    bool? isNotificationGranted,
    bool? isNotificationOn,
  }) {
    return SettingState(
      stateStatus: stateStatus ?? this.stateStatus,
      isNotificationGranted: isNotificationGranted ?? this.isNotificationGranted,
      isNotificationOn: isNotificationOn ?? this.isNotificationOn,
    );
  }
}
