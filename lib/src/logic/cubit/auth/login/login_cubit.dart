import 'package:tech/index.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginState());
  final AuthService _service = AppService.I.get<AuthService>();

  void login(LoginReqModel reqModel) async {
    if (state.stateStatus == OneStateStatus.loading) return;
    emit(state.copyWith(
      stateStatus: OneStateStatus.loading,
    ));
    final response = await _service.login(reqModel);
    emit(response.fold(
      (l) => state.copyWith(
        stateStatus: OneStateStatus.failure,
        message: l,
      ),
      (r) {
        StorageUtil.setString(ACCESS_TOKEN, r.token);
        StorageUtil.setString(REFRESH_TOKEN, r.refreshToken);
        //OneUserSharedInfo.saveUserInfo(r.items);
        return state.copyWith(
          stateStatus: OneStateStatus.success,
          data: r,
        );
      },
    ));
  }

  void reset() {
    emit(LoginState());
  }
}
