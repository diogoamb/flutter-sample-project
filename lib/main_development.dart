import 'package:tech/index.dart';
import 'package:tech/bootstrap.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await OnePackageInfo.init();
  await StorageUtil.init();
  await ConnectivityUtil.init();
  FlavorConfig(
    environment: FlavorEnvironment.DEV,
    name: DEVELOPMENT_LABEL_NAME,
    variables: developmentEnvironment,
  );
  await bootstrap(() => App());
}
